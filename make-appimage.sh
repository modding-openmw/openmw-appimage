#!/usr/bin/env bash
set -eu -o pipefail

# [appimage/stderr] More than one architectures were found of the AppDir source directory "Engine"
# [appimage/stderr] A valid architecture with the ARCH environmental variable should be provided
# [appimage/stderr] e.g. ARCH=x86_64 appimagetool ...
# ERROR: Failed to run plugin: appimage (exit code: 1)
export ARCH=x86_64

base_dir=/opt/openmw-appimage

BUILD_BRANCH="${BUILD_BRANCH:-none}"
BUILD_SHA="${BUILD_SHA:-none}"
BUILD_TAG="${BUILD_TAG:-none}"
dev_version=0.49.0
_VERSION="${_VERSION:-none}"
RWC_BUILD="${RWC_BUILD-no}"

# osg-openmw and bullet are implicitly built
if [[ $BUILD_BRANCH != none ]]; then
    build-openmw --install-prefix ${base_dir} --skip-install-pkgs --build-mygui --build-sdl2 --sdl-version release-2.0.16 --branch "$BUILD_BRANCH" --verbose
elif [[ $BUILD_SHA != none ]]; then
    build-openmw --install-prefix ${base_dir} --skip-install-pkgs --build-mygui --build-sdl2 --sdl-version release-2.0.16 --sha "$BUILD_SHA" --verbose
elif [[ $BUILD_TAG != none ]]; then
    build-openmw --install-prefix ${base_dir} --skip-install-pkgs --build-mygui --build-sdl2 --sdl-version release-2.0.16 --tag "$BUILD_TAG" --verbose
elif [[ $RWC_BUILD != no ]]; then
    build-openmw --install-prefix ${base_dir} --skip-install-pkgs --build-mygui --build-sdl2 --sdl-version release-2.0.16 --verbose --without-cs
else
    # Build the latest commit
    build-openmw --install-prefix ${base_dir} --skip-install-pkgs --build-mygui --build-sdl2 --sdl-version release-2.0.16 --verbose
fi

cd ${base_dir}/src/openmw/build

if [[ $RWC_BUILD != no ]]; then
    cat > org.openmw.engine.desktop <<EOF
[Desktop Entry]
Type=Application
Name=RWC (Robowind)
GenericName=Role Playing Game
Comment=An original shooter action game for OpenMW
Keywords=Morrowind;Reimplementation Mods;esm;bsa;
TryExec=openmw
Exec=openmw
Icon=openmw
Categories=Game;RolePlaying;
EOF
else
    # Generate .desktop files that don't yet exist
    cat > org.openmw.engine.desktop <<EOF
[Desktop Entry]
Type=Application
Name=OpenMW Engine
GenericName=Role Playing Game
Comment=An engine replacement for The Elder Scrolls III: Morrowind
Keywords=Morrowind;Reimplementation Mods;esm;bsa;
TryExec=openmw
Exec=openmw
Icon=openmw
Categories=Game;RolePlaying;
EOF
fi

cat > org.openmw.navmeshtool.desktop <<EOF
[Desktop Entry]
Type=Application
Name=OpenMW Navmeshtool
GenericName=Role Playing Game
Comment=Generate a navmesh database file
Keywords=Morrowind;Reimplementation Mods;esm;bsa;
TryExec=openmw-navmeshtool
Exec=openmw-navmeshtool
Icon=openmw
Categories=Game;RolePlaying;
EOF

cat > org.openmw.bsatool.desktop <<EOF
[Desktop Entry]
Type=Application
Name=OpenMW BSAtool
GenericName=Role Playing Game
Comment=An engine replacement for The Elder Scrolls III: Morrowind
Keywords=Morrowind;Reimplementation Mods;esm;bsa;
TryExec=openmw
Exec=bsatool
Icon=openmw-cs
Categories=Game;RolePlaying;
EOF

cat > org.openmw.esmtool.desktop <<EOF
[Desktop Entry]
Type=Application
Name=OpenMW ESMtool
GenericName=Role Playing Game
Comment=An engine replacement for The Elder Scrolls III: Morrowind
Keywords=Morrowind;Reimplementation Mods;esm;bsa;
TryExec=openmw
Exec=esmtool
Icon=openmw-cs
Categories=Game;RolePlaying;
EOF

cat > org.openmw.iniimporter.desktop <<EOF
[Desktop Entry]
Type=Application
Name=OpenMW INI Importer
GenericName=Role Playing Game
Comment=An engine replacement for The Elder Scrolls III: Morrowind
Keywords=Morrowind;Reimplementation Mods;esm;bsa;
TryExec=openmw
Exec=openmw-iniimporter
Icon=openmw-cs
Categories=Game;RolePlaying;
EOF

mkdir -p {CS,Engine,Iniimporter,Launcher,Navmeshtool,Bsatool,Esmtool}/usr/bin
mkdir -p {CS,Engine,Iniimporter,Launcher,Navmeshtool,Bsatool,Esmtool}/usr/share/metainfo

# Generate AppStream metadata
cat > CS/usr/share/metainfo/org.openmw.cs.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.cs</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<launchable type="desktop-id">org.openmw.cs.desktop</launchable>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.cs.desktop</id>
	</provides>
</component>
EOF

if [[ $RWC_BUILD != no ]]; then
    cat > Engine/usr/share/metainfo/org.openmw.engine.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.engine</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>RWC (Robowind)</name>
	<summary>An original shooter action game for OpenMW</summary>
	<description>
		<p>An original shooter action game for OpenMW</p>
	</description>
	<launchable type="desktop-id">org.openmw.engine.desktop</launchable>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://cdn.discordapp.com/attachments/1054317463073988668/1147490608181690368/openmw_2023-09-02_14-10-28-378.jpg</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.engine.desktop</id>
	</provides>
</component>
EOF
else
    cat > Engine/usr/share/metainfo/org.openmw.engine.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.engine</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<launchable type="desktop-id">org.openmw.engine.desktop</launchable>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.engine.desktop</id>
	</provides>
</component>
EOF
fi

cat > Launcher/usr/share/metainfo/org.openmw.launcher.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.launcher</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<launchable type="desktop-id">org.openmw.launcher.desktop</launchable>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.launcher.desktop</id>
	</provides>
</component>
EOF

cat > Navmeshtool/usr/share/metainfo/org.openmw.navmeshtool.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.navmeshtool</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.navmeshtool.desktop</id>
	</provides>
</component>
EOF

cat > Bsatool/usr/share/metainfo/org.openmw.bsatool.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.bsatool</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.bsatool.desktop</id>
	</provides>
</component>
EOF

cat > Esmtool/usr/share/metainfo/org.openmw.esmtool.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.esmtool</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.esmtool.desktop</id>
	</provides>
</component>
EOF

cat > Iniimporter/usr/share/metainfo/org.openmw.iniimporter.appdata.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>org.openmw.iniimporter</id>
	<metadata_license>MIT</metadata_license>
	<project_license>GPL-2.0+</project_license>
	<name>OpenMW</name>
	<summary>An engine replacement for The Elder Scrolls III: Morrowind</summary>
	<description>
		<p>OpenMW is an open-source open-world RPG game engine that supports playing Morrowind.</p>
	</description>
	<url type="homepage">https://openmw.org/</url>
	<screenshots>
		<screenshot type="default">
			<image>https://openmw.org/wp-content/uploads/2022/04/screenshot052.png</image>
		</screenshot>
	</screenshots>
	<provides>
		<id>org.openmw.iniimporter.desktop</id>
	</provides>
</component>
EOF

# They all get these...
defaults="${base_dir}/osg-openmw/lib/osgPlugins-3.6.5 gamecontrollerdb.txt openmw.appdata.xml openmw.cfg openmw.cfg.install resources defaults.bin"

# Package-specific items...
if [[ $RWC_BUILD != no ]]; then
    cp -r org.openmw.engine.desktop openmw $defaults \
       Engine/usr/bin/
else
    cp -r org.openmw.engine.desktop openmw $defaults \
       Engine/usr/bin/
    cp -r org.openmw.bsatool.desktop bsatool $defaults \
       Bsatool/usr/bin/
    cp -r org.openmw.esmtool.desktop esmtool $defaults \
       Esmtool/usr/bin/
    cp -r org.openmw.esmtool.desktop openmw-iniimporter $defaults \
       Iniimporter/usr/bin/
    cp -r org.openmw.navmeshtool.desktop openmw-navmeshtool $defaults \
       Navmeshtool/usr/bin/
    cp -r org.openmw.launcher.desktop bsatool esmtool niftest openmw openmw-bulletobjecttool openmw-iniimporter openmw-launcher openmw-wizard openmw-navmeshtool $defaults \
       Launcher/usr/bin/
    cp -r org.openmw.cs.desktop defaults-cs.bin openmw-cs $defaults \
       CS/usr/bin/

    mkdir -p {CS,Launcher}/usr/plugins/{platformthemes,styles}
    cp /usr/lib/x86_64-linux-gnu/qt5/plugins/platformthemes/* CS/usr/plugins/platformthemes/
    cp /usr/lib/x86_64-linux-gnu/qt5/plugins/platformthemes/* Launcher/usr/plugins/platformthemes/

    cp /usr/lib/x86_64-linux-gnu/qt5/plugins/styles/* CS/usr/plugins/styles/
    cp /usr/lib/x86_64-linux-gnu/qt5/plugins/styles/* Launcher/usr/plugins/styles/
fi
export LD_LIBRARY_PATH=${base_dir}/osg-openmw/lib:${base_dir}/bullet/lib
if [[ $_VERSION = none ]]; then
    cd ${base_dir}/src/openmw
    long_sha=$(git rev-parse HEAD)
    short_sha=$(git rev-parse --short HEAD)
    today=$(git log --pretty=format:'%ci' -1 HEAD | awk '{ print $1 $2 }' | tr -d "-" | tr -d ":")
    echo "{\"shortsha\":\"$short_sha\",\"date\":\"$today\",\"sha\":\"$long_sha\"}" > /mnt/latest.json
    cd -
    export VERSION="${dev_version}_dev_${today}_$short_sha"
else
    export VERSION="${_VERSION}"
fi

if [[ $RWC_BUILD != no ]]; then
    export VERSION="Demo_The_Sphere_Station"
    cp -afr /srv/RWC_Final/data /srv/RWC_Final/openmw.appdata.xml /srv/RWC_Final/openmw.cfg /srv/RWC_Final/openmw.cfg.install /srv/RWC_Final/settings.cfg /srv/RWC_Final/shaders.yaml Engine/usr/bin/
    cp -f Engine/usr/bin/data/data\ files/rwc.png Engine/usr/bin/openmw.png
    mv Engine/usr/bin/data/Lua Engine/usr/bin/data/lua
    sed -i "s|\\\|/|g" Engine/usr/bin/data/openmw.cfg
    sed -i "s|user-data=user_data|user-data=\"?userdata?RWC\"|;s|config=user_data|config=\"?userconfig?RWC\"|" Engine/usr/bin/openmw.cfg
    /opt/linuxdeploy-x86_64.AppImage \
        --appdir=Engine --output=appimage \
        --desktop-file=org.openmw.engine.desktop \
        --deploy-deps-only=Engine/usr/bin \
        --icon-file=Engine/usr/bin/openmw.png \
        --plugin=checkrt > appimage-rwc.log
    mv appimage-*.log /mnt/
    mv RWC*.AppImage /mnt/
    exit $?
fi

#TODO: appimagelint

# Standalone engine
/opt/linuxdeploy-x86_64.AppImage \
    --appdir=Engine --output=appimage \
    --desktop-file=org.openmw.engine.desktop \
    --deploy-deps-only=Engine/usr/bin \
    --icon-file=../files/launcher/images/openmw.png \
    --plugin=checkrt > appimage-engine.log

# Standalone navmeshtool
/opt/linuxdeploy-x86_64.AppImage \
    --appdir=Navmeshtool \
    --output=appimage \
    --desktop-file=org.openmw.navmeshtool.desktop \
    --deploy-deps-only=Navmeshtool/usr/bin \
    --icon-file=../files/launcher/images/openmw.png \
    --plugin=checkrt > appimage-navmeshtool.log

# Standalone bsatool
/opt/linuxdeploy-x86_64.AppImage \
    --appdir=Bsatool \
    --output=appimage \
    --desktop-file=org.openmw.bsatool.desktop \
    --deploy-deps-only=Bsatool/usr/bin \
    --icon-file=../files/opencs/openmw-cs.png \
    --plugin=checkrt > appimage-bsatool.log

# Standalone esmtool
/opt/linuxdeploy-x86_64.AppImage \
    --appdir=Esmtool \
    --output=appimage \
    --desktop-file=org.openmw.esmtool.desktop \
    --deploy-deps-only=Esmtool/usr/bin \
    --icon-file=../files/opencs/openmw-cs.png \
    --plugin=checkrt > appimage-esmtool.log

# Standalone ini importer
/opt/linuxdeploy-x86_64.AppImage \
    --appdir=Iniimporter \
    --output=appimage \
    --desktop-file=org.openmw.iniimporter.desktop \
    --deploy-deps-only=Iniimporter/usr/bin \
    --icon-file=../files/opencs/openmw-cs.png \
    --plugin=checkrt > appimage-iniimporter.log

# Qt stuff
/opt/linuxdeploy-x86_64.AppImage \
    --appdir=Launcher \
    --output=appimage \
    --desktop-file=org.openmw.launcher.desktop \
    --deploy-deps-only=Launcher/usr/bin \
    --icon-file=../files/launcher/images/openmw.png \
    --plugin=qt --plugin=checkrt > appimage-launcher.log

/opt/linuxdeploy-x86_64.AppImage \
    --appdir=CS \
    --output=appimage \
    --desktop-file=org.openmw.cs.desktop \
    --deploy-deps-only=CS/usr/bin \
    --icon-file=../files/opencs/openmw-cs.png \
    --plugin=qt --plugin=checkrt > appimage-cs.log

mv appimage-*.log /mnt/
mv OpenMW*.AppImage /mnt/
