VERSION := $$(git describe --tags)

all: clean build run

clean:
	rm -fv out/*

build:
	docker build -t openmwappimage:$(VERSION) .

run:
	docker run --name openmwappimage --cap-add SYS_ADMIN --device /dev/fuse --rm -v $$(pwd)/out:/mnt openmwappimage:$(VERSION)

sh:
	docker run --name openmwappimage --cap-add SYS_ADMIN --device /dev/fuse -it --rm -v $$(pwd)/out:/mnt openmwappimage:$(VERSION) bash

rwc:
	docker run --name openmwappimage -e RWC_BUILD=y --cap-add SYS_ADMIN --device /dev/fuse --rm -v $$(pwd)/out:/mnt -v $$HOME/games/openmw/Mods/OriginalGames/RWC:/srv openmwappimage:$(VERSION)

openmw-0.48:
	docker run --name openmwappimage -e BUILD_BRANCH=openmw-48 -e _VERSION="0.48.0" --cap-add SYS_ADMIN --device /dev/fuse --rm -v $$(pwd)/out:/mnt openmwappimage:$(VERSION)
