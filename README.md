# openmw-appimage

Build an AppImage for OpenMW

## Requirements

* docker
* FUSE
* GNU make (optional)

## Usage

To build:

```sh
git clone --recursive https://gitlab.com/modding-openmw/openmw-appimage
cd openmw-appimage
make # Read the Makefile to skip make and properly call docker directly
```

Four AppImages (and their build logs) will be placed into a `out` directory in the root of this repository: one for the `openmw` executable (named "Engine"), one for the `openmw-navmeshtool` executable (named "Navmeshtool"), one for the `openmw-launcher` executable (named "Launcher"), and a fourth for the `openmw-cs` executable (named "Content Editor").

To get a shell inside the container (useful for developing):

```sh
git clone --recursive https://gitlab.com/modding-openmw/openmw-appimage
cd openmw-appimage
make sh # Read the Makefile to skip make and properly call docker directly
```
