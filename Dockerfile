FROM docker.io/library/ubuntu:jammy-20230425
# "jammy" is 22.04

# System stuff
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    appstream binutils build-essential ca-certificates cmake curl file fuse git git libavcodec-dev libavformat-dev libavutil-dev libboost-filesystem-dev libboost-filesystem-dev libboost-iostreams-dev libboost-program-options-dev libboost-program-options-dev libboost-system-dev libcollada-dom-dev libfontconfig1-dev libfreetype6-dev libgl1-mesa-dev libicu-dev libjpeg-dev liblz4-dev libopenal-dev libpng-dev libqt5svg5-dev libsqlite3-dev libswresample-dev libswscale-dev libtinyxml-dev libunshield-dev libxinerama-dev libxrandr-dev libxt-dev libyaml-cpp-dev make mold pkg-config qt5ct qtbase5-dev qttools5-dev software-properties-common unzip && \
    add-apt-repository -y ppa:openmw/openmw && \
    apt-get update && \
    apt-get install -y libluajit-5.1-dev

# Cleanup
RUN apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/apt /var/lib/cache /var/lib/log

# AppImage tooling
WORKDIR /opt
RUN curl -sLO https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
RUN curl -sLO https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage
RUN curl -sLO https://github.com/linuxdeploy/linuxdeploy-plugin-checkrt/releases/download/continuous/linuxdeploy-plugin-checkrt-x86_64.sh
RUN chmod +x linuxdeploy*.AppImage linuxdeploy-plugin-checkrt-x86_64.sh

# For building stuff
COPY build-openmw/build-openmw.py /
RUN ln -s /build-openmw.py /usr/bin/build-openmw

WORKDIR /
# Cache the openmw source
RUN git clone https://gitlab.com/OpenMW/openmw /opt/openmw-appimage/src/openmw

# Build and cache deps
RUN build-openmw --install-prefix /opt/openmw-appimage --skip-install-pkgs --verbose --only-bullet
RUN build-openmw --install-prefix /opt/openmw-appimage --skip-install-pkgs --verbose --only-mygui
RUN build-openmw --install-prefix /opt/openmw-appimage --skip-install-pkgs --verbose --only-osg-openmw
RUN build-openmw --install-prefix /opt/openmw-appimage --skip-install-pkgs --verbose --only-sdl --sdl-version release-2.0.22

# Make the appimage
COPY make-appimage.sh /
CMD [ "/make-appimage.sh" ]
